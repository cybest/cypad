<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Card::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'collapsed' => rand(0, 1),
        'archived' => rand(0, 1),
    ];
});


$factory->define(App\Element::class, function (Faker\Generator $faker) {
    
    $elementableTypes = [
//        'AudioElement',       
        'ChecklistElement',
        'ImageElement',
        'TableElement',
        'TextElement',
        'TimerElement',
//        'VideoElement',      
    ];

    $generatedElementableType = $elementableTypes[array_rand($elementableTypes)];

    switch ($generatedElementableType) {

//        case 'AudioElement':
//            $generatedElementable = new App\AudioElement;
//
//            $uploadPath = '/upload/audio';
//
//            /* $uri = $faker->video(public_path() . $uploadPaht); */
//
//            $generatedElementable->uri = $uri;
//            $generatedElementable->filename = basename($uri);
//
//            $generatedElementable->save();
//            break;

        case 'ChecklistElement':
            $generatedElementable = new App\ChecklistElement;
            $generatedElementable->save();

            $checklistRowsNumber = rand(1, 10);
            for ($row = 0; $row < $checklistRowsNumber; $row++) {
                DB::table('checklist_element_items')->insert([
                    'content' => $faker->paragraph(),
                    'checked' => (bool) rand(0, 1),
                    'position' => $row,
                    'checklist_element_id' => $generatedElementable->id,
                ]);
            }

            break;
        
        case 'ImageElement':
            $generatedElementable = new App\ImageElement;

            $uploadPath = '/upload/img';
            $absoluteUri = $faker->image(public_path() . $uploadPath);
            $filename = basename($absoluteUri);
            $generatedElementable->uri = $uploadPath . '/' . $filename;
            $generatedElementable->filename = $filename;

            $generatedElementable->save();
            break;

        case 'TableElement':
            $generatedElementable = new App\TableElement;
            $generatedElementable->save();

            $tableWidth = rand(2, 4);
            $tableLength = rand(2, 10);

            for ($row = 0; $row < $tableLength; $row++) {
                for ($column = 0; $column < $tableWidth; $column++) {

                    DB::table('table_element_cells')->insert([
                        'content' => $faker->sentence(),
                        'table_element_id' => $generatedElementable->id,
                        'row' => $row,
                        'column' => $column,
                    ]);
                }
            }

            break;

        case 'TextElement':
            $generatedElementable = new App\TextElement;

            $textTypes = ['default', 'primary', 'success', 'info', 'warning', 'danger'];
            $generatedTextType = $textTypes[array_rand($textTypes)];
            $generatedElementable->type = $generatedTextType;
            $generatedElementable->content = $faker->text();

            $generatedElementable->save();
            
            break;

        case 'TimerElement':
            $generatedElementable = new App\TimerElement;
            $generatedElementable->save();

            $timepointsNumber = rand(1, 10);
            $actionTypes = ['start', 'stop', 'set'];
            for ($i = 0; $i < $timepointsNumber; $i++) {
                $action = $actionTypes[array_rand($actionTypes)];
                DB::table('timer_element_times')->insert([
                    'timepoint' => $faker->dateTime(),
                    'interval'  => rand(-(60 * 60 * 24), +(60 * 60 * 24)),
                    'action' => $action,
                    'timer_element_id' => $generatedElementable->id,
                ]);
            }

            break;

//        case 'VideoElement':
//            $generatedElementable = new App\VideoElement;
//
//            $uploadPath = '/upload/video';
//
//            /* $uri = $faker->video(public_path() . $uploadPaht); */
//
//            $generatedElementable->uri = $uri;
//            $generatedElementable->filename = basename($uri);
//
//            $generatedElementable->save();
//            break;
    
    }

    $card = App\Card::orderBy(DB::raw('RAND()'))->first();

    $elementsCount = $card->elements->count();

    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(),
        'elementable_id' => $generatedElementable->id,
        'elementable_type' => 'App\\' . $generatedElementableType,
        'card_id' => $card->id,
        'position' => $elementsCount,    // Would work (for sure) only if one element is created at the time ...
                                         // run factory('App\Element')->create() within for loop to obtain required number of elements
        'collapsed' => rand(0, 1),
        'archived' => rand(0, 1),
    ];
});
