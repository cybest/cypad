<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistElementItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_element_items', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->boolean('checked')->default(false);
            $table->integer('position')->unsigned();
            $table->boolean('archived')->default(false);
            $table->integer('checklist_element_id')->unsigned();
            $table->foreign('checklist_element_id')->references('id')->on('checklist_elements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checklist_element_items');
    }
}
