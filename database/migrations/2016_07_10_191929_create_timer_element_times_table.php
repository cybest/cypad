<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimerElementTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timer_element_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('timer_element_id')->unsigned();
            $table->foreign('timer_element_id')->references('id')->on('timer_elements');
            $table->timestamp('timepoint');
            $table->integer('interval');
            $table->string('action'); // 'start', 'stop', 'set'
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timer_element_times');
    }
}
