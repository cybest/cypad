<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->boolean('show_title')->default(true);
            $table->text('description');
            $table->boolean('show_description')->default(true);
            $table->integer('elementable_id');
            $table->string('elementable_type'); // 'App\TextElement', 'App\ImageElement', 'App\AudioElement', 'App\VideoElement', 'App\TableElement', 'App\ChecklistElement', 'App\TimerElement',
            $table->integer('card_id')->unsigned();
            $table->foreign('card_id')->references('id')->on('cards');
            $table->integer('position')->unsigned(); // Position within the card
            $table->boolean('collapsed')->default(false);
            $table->boolean('archived')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('elements');
    }
}
