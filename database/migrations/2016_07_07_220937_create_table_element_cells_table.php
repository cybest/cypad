<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableElementCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_element_cells', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('table_element_id')->unsigned();
            $table->foreign('table_element_id')->references('id')->on('table_elements');
            $table->integer('row')->unsigned();
            $table->integer('column')->unsigned();
            $table->boolean('archived')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('table_element_cells');
    }
}
