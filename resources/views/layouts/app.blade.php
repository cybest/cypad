<!DOCTYPE html>
<html lang="en">

@section('head')
    <head>
        <title>Cypad</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}">

        <!-- Material Design fonts -->

        <!-- Failback for online Roboto and Material Icons fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">

        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/icon?family=Material+Icons">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">

        <!-- Bootstrap Material Design -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/paper/bootstrap-material-design.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/paper/ripples.css') }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
@show

<body>
<div id="cypad">

    <header class="header">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">

                @if (Auth::check())
                    <ul class="nav navbar-nav">
                        <li><a @click.href="showFilterModal">Filter</a></li>
                        <li><a @click.href="showSortModal">Sort</a></li>
                        <li><a href="{{ url('/export') }}">Export</a></li>
                        <li class="divider-vertical"></li>
                    </ul>

                    <form class="navbar-form navbar-left" onsubmit="alert('Sorry, search functionality is not implemented yet ...'); return false">
                        <div class="form-group">
                            <input type="text" class="form-control col-sm-8" placeholder="Search">
                            <a href="#"><i class="material-icons">search</i></a>
                        </div>
                    </form>

                @else
                    <ul class="nav navbar-nav">
                        <li class="{{ (Request::is('/') ? 'active' : '') }}"><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Welcome</a></li>
                    </ul>
                @endif

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li class="{{ (Request::is('login') ? 'active' : '') }}"><a href="{{ url('login') }}">Login</a></li>
                        <li class="{{ (Request::is('register') ? 'active' : '') }}"><a href="{{ url('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{  Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ url('/logout') }}">Log out</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
    </header>

    @yield('content')

    @if (Auth::check())
        <footer class="footer">
            <div class="container-fluid">
                <nav class="navbar navbar-default navbar-fixed-bottom">

                    <ul class="nav navbar-nav">

                        <li><a @click.href="fireCreateCard" role="button">New card</a></li>
                        <li class="dropdown" v-show="state.activeCardId">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">New element <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a @click.href="fireCreateElement('checklist')"><i class="fa fa-list-ul" aria-hidden="true"></i> Checklist</a></li>
                                <li><a @click.href="fireCreateElement('image')"><i class="fa fa-picture-o" aria-hidden="true"></i> Image</a></li>
                                <li><a @click.href="fireCreateElement('table')"><i class="fa fa-table" aria-hidden="true"></i> Table</a></li>
                                <li><a @click.href="fireCreateElement('text')"><i class="fa fa-file-text-o" aria-hidden="true"></i> Text</a></li>
                                <li><a @click.href="fireCreateElement('timer')"><i class="fa fa-clock-o" aria-hidden="true"></i> Timer</a></li>
                            </ul>
                        </li>

                    </ul>

                </nav>
            </div>
        </footer>
    @endif

    @yield('modals')

</div>

@section('scripts')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/paper/ripples.js') }}"></script>
    <script src="{{ asset('js/paper/material.js') }}"></script>

    <script>
        $(function () {
            $.material.init();
        });
    </script>
@show

</body>
</html>
