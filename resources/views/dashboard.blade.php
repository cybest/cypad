@extends('layouts.app')

@section('content')

    <div class="container">
        <main class="main">

            <cards :cards="cards" :state="state" :filtered-in="filter.filteredInCards"></cards>

        </main>
    </div>

@endsection

@section('modals')
    <div class="modal fade" id="modalFilterCards">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Set filter</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="selectFilterType" class="control-label">Filter by</label>
                        <select id="selectFilterType" class="form-control" v-model="filter.type">
                            <option selected>Created</option>
                            <option>Updated</option>
                        </select>
                    </div>

                    <strong>Before</strong>
                    <div v-show="filter.type == 'Created'">
                        <div class="form-group">
                            <label for="createdBeforeDateFilter" class="control-label">Date</label>
                            <input type="date" class="form-control" id="createdBeforeDateFilter"
                                   v-model="filter.createdBefore.date">
                        </div>
                        <div class="form-group">
                            <label for="createdBeforeTimeFilter" class="control-label">Time</label>
                            <input type="time" class="form-control" id="createdBeforeTimeFilter"
                                   v-model="filter.createdBefore.time">
                        </div>
                    </div>
                    <div v-show="filter.type == 'Updated'">
                        <div class="form-group">
                            <label for="updatedBeforeDateFilter" class="control-label">Date</label>
                            <input type="date" class="form-control" id="updatedBeforeDateFilter"
                                   v-model="filter.updatedBefore.date">
                        </div>
                        <div class="form-group">
                            <label for="updatedBeforeTimeFilter" class="control-label">Time</label>
                            <input type="time" class="form-control" id="updatedBeforeTimeFilter"
                                   v-model="filter.updatedBefore.time">
                        </div>
                    </div>

                    <strong>After</strong>
                    <div v-show="filter.type == 'Created'">
                        <div class="form-group">
                            <label for="createdAfterDateFilter" class="control-label">Date</label>
                            <input type="date" class="form-control" id="createdAfterDateFilter"
                                   v-model="filter.createdAfter.date">
                        </div>
                        <div class="form-group">
                            <label for="createdAfterTimeFilter" class="control-label">Time</label>
                            <input type="time" class="form-control" id="createdAfterTimeFilter"
                                   v-model="filter.createdAfter.time">
                        </div>
                    </div>
                    <div v-show="filter.type == 'Updated'">
                        <div class="form-group">
                            <label for="updatedAfterDateFilter" class="control-label">Date</label>
                            <input type="date" class="form-control" id="updatedAfterDateFilter"
                                   v-model="filter.updatedAfter.date">
                        </div>
                        <div class="form-group">
                            <label for="updatedAfterTimeFilter" class="control-label">Time</label>
                            <input type="time" class="form-control" id="updatedAfterTimeFilter"
                                   v-model="filter.updatedAfter.time">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="filterCards">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalNoFilterCardsInfo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Incorrect filter limits</h4>
                </div>

                <div class="modal-body">
                    No intersection for chosen before and after filter criteria
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSortCards">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Sort cards</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="selectSortCriterion" class="control-label">Sort by</label>
                        <select id="selectSortCriterion" class="form-control" v-model="sorting.criterion">
                            <option selected>Created</option>
                            <option>Updated</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="selectSortCriterion" class="control-label">Order</label>
                        <select id="selectSortCriterion" class="form-control" v-model="sorting.order">
                            <option selected>Oldest first</option>
                            <option>Newest first</option>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="sortCards">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSelectElementable">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Select element type</h4>
                </div>

                <div class="modal-body">
                    <a @click.href="fireCreateElement('checklist')" class="btn btn-raised btn-block" data-dismiss="modal">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> Checklist
                    </a>
                    <a @click.href="fireCreateElement('image')" class="btn btn-raised btn-block" data-dismiss="modal">
                        <i class="fa fa-picture-o" aria-hidden="true"></i> Image
                    </a>
                    <a @click.href="fireCreateElement('table')" class="btn btn-raised btn-block" data-dismiss="modal">
                        <i class="fa fa-table" aria-hidden="true"></i> Table
                    </a>
                    <a @click.href="fireCreateElement('text')" class="btn btn-raised btn-block" data-dismiss="modal">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Text
                    </a>
                    <a @click.href="fireCreateElement('timer')" class="btn btn-raised btn-block" data-dismiss="modal">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> Timer
                    </a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script>
        window.csrf_token = '{{ csrf_token() }}';
    </script>

    <script src="{{ asset('js/main.js') }}"></script>
@endsection