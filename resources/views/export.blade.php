<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style>
        table {
            width: 100%;
        }

        .title {
            text-align: center;
            text-decoration: underline;
        }

        .archived {
            opacity: 0.5;
        }

        .checklist-element {
            font-size: 1.2em;
            padding: 10px 0;
        }

        .element-table {
            border-top: 1px dotted silver;
            border-bottom: 1px dotted silver;
            font-size: 1.2em;
        }

        .table-element table {
            border-collapse: collapse;
            font-size: 1.2em;
        }

        .table-element table th,
        .table-element table td {
            border: 1px solid black;
            padding: 5px;
        }

        .text-element {
            background: silver;
            padding: 10px;
            font-size: 1.2em;
        }

        .timer-element {
            padding: 10px;
        }

        .timer-element table td {
            font-size: 1.2em;
            text-align: center;
        }

        .timer-element .timer-history {
            opacity: 0.5;
        }
    </style>
</head>
<body>
    <h1 class="title">{{ $title }}</h1>
    <hr>
    @foreach($cards as $card)
        <table class={{ $card->archived ? 'archived' : '' }}>
            <thead>
                <tr>
                    <th>
                        <h2>{{ $card->title }}</h2>
                        <p>{{ $card->description }}</p>
                    </th>
                </tr>
                <tr>
                    <th>
                        <p><em>Created at: {{ $card->created_at }}</em></p>
                        <p><em>Updated at: {{ $card->updated_at }}</em></p>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($card->elements as $element)
                <tr class={{ $element->archived ? 'archived' : '' }}>
                    <td>
                        <table class="element-table">
                            <thead>
                                <tr>
                                    <td>
                                        <h3>{{ $element->title }}</h3>
                                        <p>{{ $element->description }}</p>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        @if($element->elementable_type == 'App\ChecklistElement')
                                            <table>
                                                @foreach($element->elementable->checklistData as $item)
                                                    <tr>
                                                        <td>
                                                            <div class="checklist-element">
                                                                <label>
                                                                    <input type="checkbox" {{ $item->checked ? 'checked' : '' }}>
                                                                    {{ $item->content }}
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @endif

                                        @if($element->elementable_type == 'App\ImageElement')
                                            <div class="image-element">
                                                <img src="{{ $element->elementable->filename }}">
                                            </div>
                                        @endif

                                        @if($element->elementable_type == 'App\TableElement')
                                            <div class="table-element">
{{--                                                {{ dd($element->elementable->tableData['body']) }}--}}

                                                <table>
                                                    <thead>
                                                        <tr>
                                                            @for($column = 0; $column < $element->elementable->tableData['columns']; $column++)
                                                                <th>
                                                                    {{ $element->elementable->tableData['head'][$column]->content }}
                                                                </th>
                                                            @endfor
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @for($row = 0; $row < $element->elementable->tableData['rows']; $row++)
                                                            <tr>
                                                                @for($column = 0; $column < $element->elementable->tableData['columns']; $column++)
                                                                    <td>{{ $element->elementable->tableData['body'][$row][$column]->content }}</td>
                                                                @endfor
                                                            </tr>
                                                        @endfor
                                                    </tbody>
                                                </table>

                                            </div>
                                        @endif

                                        @if($element->elementable_type == 'App\TextElement')
                                            <div class="text-element">
                                                <p><em>{{ strtoupper($element->elementable->type) }}</em></p>
                                                <p>{{ $element->elementable->content }}</p>
                                            </div>
                                        @endif

                                        @if($element->elementable_type == 'App\TimerElement')
                                            <div class="timer-element">
                                                <h3>Current timer state:</h3>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>Time point</th>
                                                            <th>Action</th>
                                                            <th>Interval (s)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $element->elementable->timerData['current']->timepoint }}</td>
                                                            <td>{{ $element->elementable->timerData['current']->action }}</td>
                                                            <td>{{ $element->elementable->timerData['current']->interval }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="timer-history">
                                                    <h3>Previous timer states:</h3>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <th>Time point</th>
                                                                <th>Action</th>
                                                                <th>Interval</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($element->elementable->timerData['history'] as $timerTime)
                                                                <tr>
                                                                    <td>{{ $timerTime->timepoint }}</td>
                                                                    <td>{{ $timerTime->action }}</td>
                                                                    <td>{{ $timerTime->interval }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <hr>
    @endforeach
</body>
</html>