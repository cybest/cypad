var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

import Cards from './components/Cards.vue';

var vm = new Vue({
   el: '#cypad',

    components: { Cards },

    data () {
        return {
            cards: [],

            state: {
                activeCardId: null,
                activeElementId: null,
                activeElementIds: []
            },

            filter: {
                type: '',

                createdBefore: {
                    date: '',
                    time: ''
                },

                createdAfter: {
                    date: '',
                    time: ''
                },

                updatedBefore: {
                    date: '',
                    time: ''
                },

                updatedAfter: {
                    date: '',
                    time: ''
                },

                filteredInCards: [],
            },

            sorting: {
                criterion: '',

                order: ''
            }
        }
    },

    ready () {
        this.fetchCardList();
        console.info('Application is ready.');
    },

    methods: {
        fetchCardList () {
            var vm = this;

            this.$http.get('cards').then((response) => {
                var fetchedCards = JSON.parse(response.body).cards;

                for (let key in fetchedCards) {
                    this.cards.push(fetchedCards[key]);
                }

                this.cards.forEach(this.sortCardElements);

                let createdMin = '';
                let createdMax = '';
                let updatedMin = '';
                let updatedMax = '';

                this.cards.forEach(function (card, index) {
                    vm.filter.filteredInCards[index] = true;

                    if (card.created_at && card.created_at < createdMin || ! createdMin) createdMin = card.created_at;
                    if (card.created_at && card.created_at > createdMax || ! createdMax) createdMax = card.created_at;
                    if (card.updated_at && card.updated_at < updatedMin || ! updatedMin) updatedMin = card.updated_at;
                    if (card.updated_at && card.updated_at > updatedMax || ! updatedMax) updatedMax = card.updated_at;
                });

                if ( ! createdMin) createdMin = '1970-01-01 00:00:00';
                if ( ! createdMax) createdMax = new Date().toISOString().slice(0, 19).replace('T', ' ');
                if ( ! updatedMin) updatedMin = '1970-01-01 00:00:00';
                if ( ! updatedMax) updatedMax = new Date().toISOString().slice(0, 19).replace('T', ' ');

                this.filter.createdBefore.date = createdMax.slice(0, 10);
                this.filter.createdBefore.time = createdMax.slice(11);

                this.filter.createdAfter.date = createdMin.slice(0, 10);
                this.filter.createdAfter.time = createdMin.slice(11);

                this.filter.updatedBefore.date = updatedMax.slice(0, 10);
                this.filter.updatedBefore.time = updatedMax.slice(11);

                this.filter.updatedAfter.date = updatedMin.slice(0, 10);
                this.filter.updatedAfter.time = updatedMin.slice(11);
            }, (response) => {
                console.error('Downloading the cards failed!');
            });
        },

        showFilterModal () {
            $('#modalFilterCards').modal();
        },

        showSortModal () {
            $('#modalSortCards').modal();
        },
        
        filterCards () {
            console.info('>>> filterCards');
            let filterType = this.filter.type;
            let filterCreatedBefore = (this.filter.createdBefore.date + ' ' + this.filter.createdBefore.time).trim();
            let filterCreatedAfter = (this.filter.createdAfter.date + ' ' + this.filter.createdAfter.time).trim();

            let filterUpdatedBefore = (this.filter.updatedBefore.date + ' ' + this.filter.updatedBefore.time).trim();
            let filterUpdatedAfter = (this.filter.updatedAfter.date + ' ' + this.filter.updatedAfter.time).trim();

            let vm = this;

            if (filterType == 'Created' && filterCreatedBefore >= filterCreatedAfter ||
                filterType == 'Updated' && filterUpdatedBefore >= filterUpdatedAfter) {

                this.cards.forEach(function (card, index) {
                    
                    switch (filterType) {
                        case 'Created':
                            if (card.created_at >= filterCreatedAfter && card.created_at <= filterCreatedBefore) {
                                vm.filter.filteredInCards.$set(index, true);
                            } else {
                                vm.filter.filteredInCards.$set(index, false);
                            }
                            break;
                        case 'Updated':
                            if (card.updated_at >=filterUpdatedAfter && card.updated_at <= filterUpdatedBefore) {
                                vm.filter.filteredInCards.$set(index, true);
                            } else {
                                vm.filter.filteredInCards.$set(index, false);
                            }
                            break;
                    }
                });
            } else {
                $('#modalNoFilterCardsInfo').modal();
            }
        },

        sortCards () {
            console.info('>>> sortCards');
            
            vm = this;

            this.cards.sort(function (a, b) {
                switch (vm.sorting.criterion) {
                    case 'Created':
                        switch (vm.sorting.order) {
                            case 'Oldest first':
                                if (a.created_at < b.created_at) return -1;
                                if (a.created_at > b.created_at) return 1;
                                return 0;
                                break;
                            case 'Newest first':
                                if (b.created_at < a.created_at) return -1;
                                if (b.created_at > a.created_at) return 1;
                                return 0;
                                break;
                        }
                        break;
                    case 'Updated':
                        switch (vm.sorting.order) {
                            case 'Oldest first':
                                if (a.updated_at < b.updated_at) return -1;
                                if (a.updated_at > b.updated_at) return 1;
                                return 0;
                            case 'Newest first':
                                if (b.updated_at < a.updated_at) return -1;
                                if (b.updated_at > a.updated_at) return 1;
                                return 0;
                                break;
                        }
                        break;
                }
            });
        },

        sortCardElements (card) {
            card.elements.sort(function (a, b) {
                return a.position - b.position;
            });
        },

        fireCreateCard () {
            this.$broadcast('createCard');
        },

        fireCreateElement (elementableType) {
            this.$broadcast('createElement', elementableType);
        },
    }
});
