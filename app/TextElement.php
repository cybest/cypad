<?php

namespace App;

class TextElement extends Elementable
{
    protected $fillable = [
        'type', 'content',
    ];
}
