<?php

namespace App;

class ChecklistElement extends Elementable
{
    /**
     * Get the items of the checklist element.
     */
    public function items()
    {
        return $this->hasMany('App\ChecklistElementItem');
    }
}
