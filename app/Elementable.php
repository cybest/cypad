<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elementable extends Model
{
    /**
     * Get the text element's wrapper element.
     */
    public function wrapper()
    {
        return $this->morphOne('App\Element', 'elementable');
    }
}
