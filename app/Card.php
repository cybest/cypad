<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'title','description', 'collapsed', 'archived',
    ];
    
    /**
     * Get the elements on the card.
     */
    public function elements()
    {
        return $this->hasMany('App\Element');
    }
}
