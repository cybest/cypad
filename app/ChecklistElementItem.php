<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistElementItem extends Model
{
    protected $fillable = [
        'content', 'checked', 'position', 'archived', 'checklist_element_id',
    ];

    /**
     * Get the parent checklist element owning the checklist item.
     */
    public function checklistElement()
    {
        return $this->belongsTo('App\ChecklistElement');
    }
}
