<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimerElementTime extends Model
{
    protected $fillable = [
        'timer_element_id', 'timepoint', 'interval', 'action',
    ];
    
    /**
     * Get the parent timer element owning the time record.
     */
    public function timerElement()
    {
        return $this->belongsTo('App\TimerElement');
    }
}
