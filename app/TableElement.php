<?php

namespace App;

use DB;

class TableElement extends Elementable
{
    /**
     * Get the cells of the table element.
     */
    public function cells()
    {
        return $this->hasMany('App\TableElementCell');
    }
}
