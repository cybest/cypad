<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CardController extends Controller
{

    /**
     * Sort and format checklist items
     *
     * @param \Illuminate\Support\Collection $items
     * @return \Illuminate\Support\Collection
     */
    private function formatChecklistElementData(\Illuminate\Support\Collection $items)
    {
        $checklistData = $items->sortBy('position')->values();

        return $checklistData;
    }
    
    /**
     * Format table cells into rows and columns
     *
     * @param \Illuminate\Support\Collection $cells
     * @return \Illuminate\Support\Collection
     */
    private function formatTableElementData(\Illuminate\Support\Collection $cells)
    {
        $tableData = collect([
            'head' => collect(),
            'body' => collect(),
        ]);

        $rowsCount = $columnsCount = 0;

        foreach ($cells as $cell) {
            if ($cell->row == 0) {

                $tableData['head'][$cell->column] = $cell;

                if ($cell->column >= $columnsCount) {
                    $columnsCount = $cell->column;
                }

            } else {
                if ( ! isset($tableData['body'][$cell->row - 1])) {
                    $tableData['body'][$cell->row - 1] = collect();
                }

                $tableData['body'][$cell->row - 1][$cell->column] = $cell;

                if ($cell->column >= $columnsCount) {
                    $columnsCount = $cell->column + 1;
                }

                if ($cell->row > $rowsCount) {
                    $rowsCount = $cell->row;
                }
            }
        }

        $tableData['rows'] = $rowsCount;
        $tableData['columns'] = $columnsCount;

        return $tableData;
    }

    /**
     * Sort and format timer times
     *
     * @param \Illuminate\Support\Collection $times
     * @return \Illuminate\Support\Collection
     */
    private function formatTimerElementData(\Illuminate\Support\Collection $times)
    {
        $historyTimes = $times->sortBy('timepoint')->values();

        if ($historyTimes) {
            $currentTime =  $historyTimes->pop();
        } else {
            $currentTime = null;
        }

        $timerData = collect([
            'current' => $currentTime,
            'history' => $historyTimes,
        ]);

        return $timerData;
    }

    /**
     * Return the collection of cards.
     *
     * @return string JSON
     */
    public function getCollection()
    {
        $data = collect([
            'cards' => Card::with('elements.elementable')->get()
        ]);

        foreach ($data['cards'] as $key => $card) {

            if (Auth::check()) {
                $userId = Auth::user()->id;
            } else {
                $userId = null;
            }

            $access = \DB::table('card_user_accesses')->where('card_id', $card->id)->where('user_id', $userId)->get();

            if (! empty($access)) {
                $card->editable = ($access[0]->access_type == 'edit');

                foreach ($card->elements as $element) {
                    switch ($element->elementable_type) {
                        case 'App\ChecklistElement':
                            $element->elementable->checklistData = $this->formatChecklistElementData($element->elementable->items);
                            unset($element->elementable->items);

                            break;
                        case 'App\TableElement':
                            $element->elementable->tableData = $this->formatTableElementData($element->elementable->cells);
                            unset($element->elementable->cells);

                            break;
                        case 'App\TimerElement':
                            $element->elementable->timerData = $this->formatTimerElementData($element->elementable->times);
                            unset($element->elementable->times);

                            break;
                    }
                }
            } else {
                $data['cards']->forget($key);
            }
        }

        return json_encode($data, JSON_NUMERIC_CHECK); // AWS hack
    }

    /**
     * Return a new card.
     *
     * @return string JSON
     */
    public function getCreate()
    {
        $card = Card::create([
            'title' => '',
            'description' => '',
        ]);

        $data = Card::with('elements')->find($card->id);

        if (Auth::check()) {
            $userId = Auth::user()->id;
        } else {
            $userId = null; // this shouldn't happen (there is no way to invoke a card creation from UI)
        }

        \DB::table('card_user_accesses')->insert(['card_id' => $card->id, 'user_id' => $userId, 'access_type' => 'edit']);

        $data->editable = true;

        return json_encode($data, JSON_NUMERIC_CHECK); // AWS hack
    }

    /**
     * Update the card record.
     *
     * @param int $id
     * @param Request $request
     * @return dateTime
     */
    public function putUpdate($id, Request $request)
    {
        $card = Card::find($id);

        $input = $request->all();

        if ($input['title'] == $card->title &&
            $input['description'] == $card->description) {
            $card->timestamps = false;
        }

        $card->update($input);
        
        return $card->updated_at;
    }
    
    /**
     * Return the single card according to its ID.
     *
     * @param  int $id
     * @return string JSON
     */
    public function getSingle($id)
    {
        // Get whole Card model (except elementable specific data
        $card = Card::with('elements.elementable')->find($id);

        $data = collect(['card' => clone $card]);

        // for each element append element specific data when applicable
        $card->elements->each(function ($element, $key) use ($data) {
            switch ($element->elementable_type) {
                case 'App\ChecklistElement':
                    $data['card']['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->items->all()]));
                    break;
                case 'App\TableElement':
                    $data['card']['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->cells->all()]));
                    break;
                case 'App\TimerElement':
                    $data['card']['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->times->all()]));
                    break;
            }
        });

        return json_encode($data, JSON_NUMERIC_CHECK); // AWS hack
    }

}