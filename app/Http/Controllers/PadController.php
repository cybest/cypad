<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Card;
use Carbon\Carbon;

class PadController extends Controller
{

    /**
     * View the user's dashboard.
     *
     * @return Response
     */
    public function dashboard()
    {
        return view('dashboard');
    }
    
    /**
     * Export zipped cards available to the logged in user
     * 
     * @return Response
     */
    public function export()
    {

        function formatChecklistElementData(\Illuminate\Support\Collection $items) {
            $checklistData = $items->sortBy('position')->values();

            return $checklistData;
        }

        function formatTableElementData(\Illuminate\Support\Collection $cells) {
            $tableData = collect([
                'head' => collect(),
                'body' => collect(),
            ]);

            $rowsCount = $columnsCount = 0;

            foreach ($cells as $cell) {
                if ($cell->row == 0) {

                    $tableData['head'][$cell->column] = $cell;

                    if ($cell->column >= $columnsCount) {
                        $columnsCount = $cell->column;
                    }

                } else {
                    if ( ! isset($tableData['body'][$cell->row - 1])) {
                        $tableData['body'][$cell->row - 1] = collect();
                    }

                    $tableData['body'][$cell->row - 1][$cell->column] = $cell;

                    if ($cell->column >= $columnsCount) {
                        $columnsCount = $cell->column + 1;
                    }

                    if ($cell->row > $rowsCount) {
                        $rowsCount = $cell->row;
                    }
                }
            }

            $tableData['rows'] = $rowsCount;
            $tableData['columns'] = $columnsCount;

            return $tableData;
        }

        function formatTimerElementData(\Illuminate\Support\Collection $times) {
            $historyTimes = $times->sortBy('timepoint')->values();

            if ($historyTimes) {
                $currentTime =  $historyTimes->pop();
            } else {
                $currentTime = null;
            }

            $timerData = collect([
                'current' => $currentTime,
                'history' => $historyTimes,
            ]);

            return $timerData;
        }

        function getCardAndImageFileNames () {
            $data = collect([
                'cards' => Card::with('elements.elementable')->get(),
                'imageFileNames' => collect([]),
            ]);

            foreach ($data['cards'] as $key => $card) {

                $access = \DB::table('card_user_accesses')->where('card_id', $card->id)->where('user_id', Auth::user()->id)->get();

                if ( ! empty($access)) {

                    foreach ($card->elements as $elementKey => $element) {

                        switch ($element->elementable_type) {
                            case 'App\ChecklistElement':
                                $element->elementable->checklistData = formatChecklistElementData($element->elementable->items);
                                unset($element->elementable->items);

                                break;
                            case 'App\ImageElement':
                                $data['imageFileNames']->push([
                                    'uri' => $element->elementable->uri,
                                    'filename' => $element->elementable->filename,
                                ]);

                                break;
                            case 'App\TableElement':
                                $element->elementable->tableData = formatTableElementData($element->elementable->cells);
                                unset($element->elementable->cells);

                                break;
                            case 'App\TimerElement':
                                $element->elementable->timerData = formatTimerElementData($element->elementable->times);
                                unset($element->elementable->times);

                                break;
                        }
                    }
                } else {
                    $data['cards']->forget($key);
                }
            }

            return $data;
        }

        if (Auth::guest()) {
            abort(403, 'Unauthorized action!');
        }

        $headers = [
            'Content-Type' => 'application/octet-stream',
        ];

        $fileName = str_slug(Auth::user()->name, '-') . '-' . str_slug(Carbon::now(), '-');

        $zipFileName = $fileName . '.zip';
        $pathToZipFile = base_path('storage/app/public/export/');

        $htmlFileName = $fileName . '.html';

        $cardAndImageFileNames = getCardAndImageFileNames();

        $imageFileNames = $cardAndImageFileNames['imageFileNames'];

        $data = [
            'title' => $htmlFileName,
            'cards' => $cardAndImageFileNames['cards'],
        ];

        $html = view('export', $data)->render();

        $zipArchive = new \ZipArchive();

        if ($zipArchive->open($pathToZipFile . $zipFileName, \ZipArchive::CREATE) === true) {
            $zipArchive->addFromString($htmlFileName, $html);

            foreach ($imageFileNames as $imageFileName) {
                $zipArchive->addFile(public_path() . $imageFileName['uri'], $imageFileName['filename']);
            }

            $zipArchive->close();
        }

        if (file_exists($pathToZipFile . $zipFileName)) {
            return response()->download($pathToZipFile . $zipFileName, $zipFileName, $headers);
        }

        abort(404, 'File does not exist!');
    }
    
}