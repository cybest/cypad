<?php

namespace App\Http\Controllers;

use DB;
use App\Card;
use App\Element;
use App\ChecklistElement;
use App\ImageElement;
use App\TableElement;
use App\TextElement;
use App\TimerElement;
use Illuminate\Http\Request;

class ElementController extends Controller
{
    /**
     * Sort and format checklist items
     *
     * @param \Illuminate\Support\Collection $items
     * @return \Illuminate\Support\Collection
     */
    private function formatChecklistElementData(\Illuminate\Support\Collection $items)
    {
        $checklistData = $items->sortBy('position')->values();

        return $checklistData;
    }

    /**
     * Format table cells into rows and columns
     *
     * @param \Illuminate\Support\Collection $cells
     * @return \Illuminate\Support\Collection
     */
    private function formatTableElementData(\Illuminate\Support\Collection $cells)
    {
        $tableData = collect([
            'head' => collect(),
            'body' => collect(),
        ]);

        $rowsCount = $columnsCount = 0;

        foreach ($cells as $cell) {
            if ($cell->row == 0) {

                $tableData['head'][$cell->column] = $cell;

                if ($cell->column >= $columnsCount) {
                    $columnsCount = $cell->column;
                }

            } else {
                if ( ! isset($tableData['body'][$cell->row - 1])) {
                    $tableData['body'][$cell->row - 1] = collect();
                }

                $tableData['body'][$cell->row - 1][$cell->column] = $cell;

                if ($cell->column >= $columnsCount) {
                    $columnsCount = $cell->column + 1;
                }

                if ($cell->row > $rowsCount) {
                    $rowsCount = $cell->row;
                }
            }
        }

        $tableData['rows'] = $rowsCount;
        $tableData['columns'] = $columnsCount;

        return $tableData;
    }

    /**
     * Sort and format timer times
     *
     * @param \Illuminate\Support\Collection $times
     * @return \Illuminate\Support\Collection
     */
    private function formatTimerElementData(\Illuminate\Support\Collection $times)
    {
        $sortedTimes = $times->sortBy('timepoint')->values();
        $currentTime = $sortedTimes->pop();

        $timerData = collect([
            'current' => $currentTime,
            'history' => $sortedTimes,
        ]);

        return $timerData;
    }

    /**
     * Return the collection of elements.
     *
     * @return string JSON
     */
    public function getCollection()
    {
        $elements = Element::with('elementable')->get();

        $data = collect(['elements' => $elements->all()]);

        $elements->each(function ($element, $key) use ($data) {
            switch ($element->elementable_type) {
                case 'App\TableElement':
                    $data['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->cells->all()]));
                    break;
                case 'App\ChecklistElement':
                    $data['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->items->all()]));
                    break;
                case 'App\TimerElement':
                    $data['elements'][$key]['elementable']->push(collect(['data' => $element->elementable->times->all()]));
                    break;
            }
        });

        return json_encode($data, JSON_NUMERIC_CHECK); // AWS hack
    }

    /**
     * Return a new element of specified type.
     *
     * @param int $cardId
     * @param string $elementableType
     * @return string JSON
     */
    public function getCreate($cardId, $elementableType)
    {
        $card = Card::findOrFail($cardId);

        $elementable = [
            'type' => 'App\\' . ucfirst($elementableType) . 'Element',
        ];

        switch ($elementableType) {
            case 'checklist':
                $generatedElementable = new ChecklistElement;
                $generatedElementable->save();
                
                $elementable['id'] = $generatedElementable->id;
                
                break;
            case 'image':
                $generatedElementable = new ImageElement;

                $generatedElementable->uri = '';
                $generatedElementable->filename = '';
                
                $generatedElementable->save();
                
                $elementable['id'] = $generatedElementable->id;
                
                break;
            case 'table':
                $generatedElementable = new TableElement;
                $generatedElementable->save();

//                $tableWidth = 1;
//                $tableLength = 2;
//
//                for ($row = 0; $row < $tableLength; $row++) {
//                    for ($column = 0; $column < $tableWidth; $column++) {
//
//                        DB::table('table_element_cells')->insert([
//                            'content' => '',
//                            'table_element_id' => $generatedElementable->id,
//                            'row' => $row,
//                            'column' => $column,
//                        ]);
//                    }
//                }

                $elementable['id'] = $generatedElementable->id;

                break;
            case 'text':
                $generatedElementable = new TextElement;

                $generatedElementable->type = 'default';
                $generatedElementable->content = '';

                $generatedElementable->save();

                $elementable['id'] = $generatedElementable->id;

                break;
            case 'timer':
                $generatedElementable = new TimerElement;
                $generatedElementable->save();

                DB::table('timer_element_times')->insert([
                    'timepoint' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'interval'  => 0,
                    'action' => 'set',
                    'timer_element_id' => $generatedElementable->id,
                ]);

                $elementable['id'] = $generatedElementable->id;
                
                break;
        }
        
        $element = Element::create([
            'title' => '',
            'description' => '',
            'elementable_id' => $elementable['id'],
            'elementable_type' => $elementable['type'],
            'card_id' => $cardId,
            'position' => $card->elements->count(),
        ]);

        $element->load('elementable');

        switch ($elementableType) {
            case 'checklist':
                $element->elementable->checklistData = $this->formatChecklistElementData($element->elementable->items);
                unset($element->elementable->items);
                
                break;
            case 'table':
                $element->elementable->tableData = $this->formatTableElementData($element->elementable->cells);
                unset($element->elementable->cells);

                break;
            case 'timer':
                $element->elementable->timerData = $this->formatTimerElementData($element->elementable->times);
                unset($element->elementable->times);

                break;
        }

        return json_encode($element, JSON_NUMERIC_CHECK);
    }

    /**
     * Update the element record
     *
     * @param int $id
     * @param Request $request
     */
    public function putUpdate($id, Request $request)
    {
        $element = Element::find($id);
        
        $input = $request->all();

        if ($input['title'] == $element->title &&
            $input['description'] == $element->description) {
            $element->timestamps = false;
        }
        
        $element->update($input);
        
        return $element->updated_at;
    }


    /**
     * Return a new checklist item.
     *
     * @param int $checklistId
     * @return string JSON
     */
    public function getCreateChecklistItem($checklistId)
    {
        $newChecklistItemId = DB::table('checklist_element_items')->insertGetId([
                'checked' => false,
                'content' => '',
                'position' => DB::table('checklist_element_items')->where('checklist_element_id', $checklistId)->count(),
                'archived' => false,
                'checklist_element_id' => $checklistId,
            ]
        );

        $newChecklistItem = DB::table('checklist_element_items')->where('id', $newChecklistItemId)->get();

        return json_encode($newChecklistItem, JSON_NUMERIC_CHECK); // AWS hack
    }

   /**
    * Update the checklist element item record
    *
    * @param int $itemId
    * @param Request $request
    * @return dateTime
    */
    public function putUpdateChecklistItem($itemId, Request $request)
    {
        $input = $request->all();

        DB::table('checklist_element_items')
            ->where('id', $itemId)
            ->update([
                'checked' => $input['checked'],
                'content' => $input['content'],
                'position' => $input['position'],
                'archived' => $input['archived']
            ]);

        $updatedChecklistElementItem = DB::table('checklist_element_items')->where('id', $itemId)->first();

        return $updatedChecklistElementItem->updated_at;
    }

   /**
    * Update the image element record
    *
    * @param int $elementId
    * @param Request $request
    * @return string JSON
    */
    public function postStoreImage($elementId, Request $request)
    {
        if ($request->file('imgfile')->isValid()) {
            $imgfile = $request->file('imgfile');
            $uploadPath = public_path() . '/upload/img/';

            do {
                $newFilename = str_random(20) . '.' . $imgfile->getClientOriginalExtension();
            } while (file_exists($uploadPath . $newFilename));

            $imgfile->move($uploadPath, $newFilename);

            $imageElement = Element::find($elementId)->elementable;

            $imageElement->update([
                'filename' => $imgfile->getClientOriginalName(),
                'uri' => '/upload/img/' . $newFilename
            ]);

            $imageData = [
                'updated_at' => $imageElement->updated_at,
                'filename' => $imageElement->filename,
                'uri' => $imageElement->uri
            ];

            return json_encode($imageData, JSON_NUMERIC_CHECK); // AWS hack
        }
    }

   /**
    * Create new table element row
    *
    * @param int $tableId
    * @return string JSON
    */
    public function getCreateTableRow($tableId)
    {
        $maxRowIndex = DB::table('table_element_cells')->where('table_element_id', $tableId)->max('row');
        $maxColumnIndex = DB::table('table_element_cells')->where('table_element_id', $tableId)->max('column');

        for($columnIndex = 0; $columnIndex < $maxColumnIndex + 1; $columnIndex++) {
            DB::table('table_element_cells')->insert([
                'content' => 'New row - column: ' . $columnIndex,
                'table_element_id' => $tableId,
                'row' => $maxRowIndex + 1,
                'column' => $columnIndex,
            ]);
        }

        $tableDataRow = DB::table('table_element_cells')
            ->where('table_element_id', $tableId)
            ->where('row', $maxRowIndex + 1)
            ->orderBy('column')->get();

        return json_encode($tableDataRow, JSON_NUMERIC_CHECK); // AWS hack
    }

    /**
     * Create new table element column
     *
     * @param int $tableId
     * @return string JSON
     */
    public function getCreateTableColumn($tableId)
    {
        $maxRowIndex = DB::table('table_element_cells')->where('table_element_id', $tableId)->max('row');
        $maxColumnIndex = DB::table('table_element_cells')->where('table_element_id', $tableId)->max('column');

        for($rowIndex = 0; $rowIndex < $maxRowIndex + 1; $rowIndex++) {
            DB::table('table_element_cells')->insert([
                'content' => 'New column - row: ' . $rowIndex,
                'table_element_id' => $tableId,
                'row' => $rowIndex,
                'column' => $maxColumnIndex + 1,
            ]);
        }

        $tableDataColumn = DB::table('table_element_cells')
            ->where('table_element_id', $tableId)
            ->where('column', $maxColumnIndex + 1)
            ->orderBy('row')->get();

        return json_encode($tableDataColumn, JSON_NUMERIC_CHECK); // AWS hack
    }

   /**
    * Update the table element record
    *
    * @param int $cellId
    * @param Request $request
    * @return dateTime
    */
    public function putUpdateTableCell($cellId, Request $request)
    {
        $input = $request->all();

        DB::table('table_element_cells')
            ->where('id', $cellId)
            ->update([
                'content' => $input['content'],
                'archived' => $input['archived'],
                'row' => $input['row'],
                'column' => $input['column'],
            ]);

        $updatedTableElementCell = DB::table('table_element_cells')->where('id', $cellId)->first();

        return $updatedTableElementCell->updated_at;
    }

   /**
    * Update the text element record
    *
    * @param int $elementId
    * @param Request $request
    * @return dateTime
    */
    public function putUpdateText($elementId, Request $request)
    {
        $textElement = Element::find($elementId)->elementable;

        $input = $request->all();
        $textElement->update($input);

        return $textElement->updated_at;
    }

   /**
    * Update the timer element record
    *
    * @param int $elementId
    * @param Request $request
    *
    * @return Array
    */
    public function postStoreTimerTime($elementId, Request $request)
    {
        $timerElement = Element::find($elementId)->elementable;

        $input = $request->all();

        $newTimerElementTimeId = DB::table('timer_element_times')->insertGetId([
                'timer_element_id' => $timerElement->id,
                'timepoint' => $input['timepoint'],
                'interval' => $input['interval'],
                'action' => $input['action'],
            ]
        );
        
        $newTimerElement = DB::table('timer_element_times')->find($newTimerElementTimeId);

        $timerTime = [
            'id' => $newTimerElementTimeId,
            'updated_at' => $newTimerElement->updated_at,
        ];

        return json_encode($timerTime, JSON_NUMERIC_CHECK); // AWS hack
    }
    
   /**
    * Return the single element according to its ID.
    *
    * @param  int  $id
    * @return string JSON
    */
    public function getSingle($id)
    {
        $element = Element::with('elementable')->find($id);

        $data = collect(['element' => clone $element]);

        switch ($element->elementable_type) {
            case 'App\TableElement':
                $data['element']['elementable']->push(collect(['data' => $element->elementable->cells->all()]));
                break;
            case 'App\ChecklistElement':
                $data['element']['elementable']->push(collect(['data' => $element->elementable->items->all()]));
                break;
            case 'App\TimerElement':
                $data['element']['elementable']->push(collect(['data' => $element->elementable->times->all()]));
                break;
        }

        return json_encode($data, JSON_NUMERIC_CHECK); // AWS hack
    }

}
