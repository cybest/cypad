<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'PadController@dashboard');

Route::get('export', 'PadController@export');

// API

// Cards
Route::get('cards', 'CardController@getCollection');

Route::get('cards/new', 'CardController@getCreate');

Route::put('cards/{id}', 'CardController@putUpdate');

Route::get('cards/{id}', 'CardController@getSingle');

// Elements
Route::get('elements', 'ElementController@getCollection');

Route::get('elements/new/{cardId}/{elementableType}', 'ElementController@getCreate');

Route::put('elements/{id}', 'ElementController@putUpdate');

Route::get('elements/{id}', 'ElementController@getSingle');

// Checklist Elements Items
Route::get('elements/checklists/{checklistId}/new', 'ElementController@getCreateChecklistItem');

Route::put('elements/checklists/{itemId}', 'ElementController@putUpdateChecklistItem');

// Image Elements
Route::post('elements/{elementId}/images/', 'ElementController@postStoreImage');

// Table Elements
Route::get('elements/tables/{tableId}/new-row', 'ElementController@getCreateTableRow');
Route::get('elements/tables/{tableId}/new-column', 'ElementController@getCreateTableColumn');

Route::put('elements/tables/cells/{cellId}', 'ElementController@putUpdateTableCell');

// Text Elements
Route::put('elements/{elementId}/texts', 'ElementController@putUpdateText');

// Timer Elements
Route::post('elements/{elementId}/timers', 'ElementController@postStoreTimerTime');
