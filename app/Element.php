<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $fillable = [
        'title', 'show_title', 'description', 'archived', 'collapsed', 'show_description', 'elementable_id', 'elementable_type', 'card_id', 'position',
    ];

    /**
     * Get the card that owns the element.
     */
    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    /**
     * Get all of the owning elementable models.
     */
    public function elementable()
    {
        return $this->morphTo();
    }
}
