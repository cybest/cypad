<?php

namespace App;

class TimerElement extends Elementable
{
    /**
     * Get the items of the timer element.
     */
    public function times()
    {
        return $this->hasMany('App\TimerElementTime');
    }
}
