<?php

namespace App;

class ImageElement extends Elementable
{
    protected $fillable = [
        'uri', 'filename',
    ];
}
