<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableElementCell extends Model
{
    protected $fillable = [
        'content', 'table_element_id', 'row', 'column',
    ];

    /**
     * Get the parent table element owning the table cell.
     */
    public function tableElement()
    {
        return $this->belongsTo('App\TableElement');
    }
}
